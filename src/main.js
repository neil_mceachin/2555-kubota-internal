import Vue from 'vue';
import VueScrollTo from 'vue-scrollto';
import axios from 'axios';
import App from './App.vue';

Vue.config.productionTip = false;

Vue.mixin({
  methods: {
    sendToTrack(action, value) {
      let doNotTrackHostname = [
        'localhost',
        'dev-2457.24preview.com',
        'qa-2457.24preview.com',
      ];
      // if (doNotTrackHostname.includes(window.location.hostname)) return;
      const data = {
        event: this.selectedEvent, // TODO Replace with variable
        product: this.products[this.selectedIndex],
        action,
        value,
      };
      // console.log(data);
      axios.post('https://2457-kubota-kiosk-tracking.social-gen.com/track.php',
        data)
        .then((response) => {
          console.log(response);
        })
        .catch((error) => {
          console.log(`error: ${error}`);
        });
    },
  }
})

new Vue({
  render: h => h(App),
}).$mount('#app');

Vue.use(VueScrollTo, {
  container: 'body',
  duration: 500,
  easing: 'ease',
  offset: 0,
  force: true,
  cancelable: true,
  onStart: false,
  onDone: false,
  onCancel: false,
  x: false,
  y: true,
});
